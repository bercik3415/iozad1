
import javax.swing.table.AbstractTableModel;
import java.util.Random;

/**
 * Klasa bedaca modelem
 */
public class MyModel{
    private final String [] actions = {"Suma", "Średnia","Min/max"};
    private TableModel myTable = new TableModel();

    // tips
    private final String [] tips = {
            " Aby zminimalizować wystarczy kliknąć -",
            " Aby wyjść wystarczy kliknąć X"
    };

    /**
     * @return tablica z nazwami akcji
     */
    public String[] getActions() {
        return actions;
    }

    /**
     * @return losowa wskazowka
     */
    public String getRandomTip() {
        return tips[new Random().nextInt(tips.length)];
    }

    /**
     * @return model tablicy
     */
    public TableModel getTableModel(){
        return myTable;
    }

    /**
     * ustawia zero w kazdej komorce Jtable
     */
    public void setZeros(){
        myTable.setZeros();
    }

    /**
     *
     * @return najwieksza wartosc w komorkach
     */
    public int max(){
        return myTable.max();
    }

    /**
     * @return najmniejsza wartosc w komorkach
    */
    public int min(){
        return myTable.min();
    }

    /**
     * @return srednia wartosc ze wszystkich komorek
     */
    public double average(){
        return myTable.average();
    }
    /**
     * @return suma obliczana poprzez dodanie wszystkich wartosci
     */
    public int sum(){
        return myTable.sum();
    }

    /**
     *
     * @param i pozycja y w talicy
     * @param j pozycja x w tablicy
     * @return wartosc w komorce w y rowie i x kolumnie
     */
    public int getValue(int i,int j){
        return (int)myTable.getValueAt(i,j);
    }

    /**
     *
     * @param val wartosc do wpisania
     * @param x pozycja x w tablicy
     * @param y pozycja y w talicy
     */
    public void setValue(int val, int x,int y){
        myTable.setValue(val,y,x);
    }


}

/**
 * klasa zawierajaca model dla tablicy
 */
class TableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final int countRowTable = 5;
    private final int countColumnTable = 5;
    private Integer[][] data = new Integer[countRowTable][countColumnTable];

    public TableModel() {
        super();
        setZeros();
    }
    public int getColumnCount() {
        return countColumnTable;
    }
    public int getRowCount() {
        return countColumnTable;
    }
    public Object getValueAt(int row, int col) {
        Object object = data[row][col];
        return object;
    }

    public void setValue(Integer value, int row, int col) {
        data[row][col] = value;
        fireTableDataChanged();
    }


    public void setZeros(){
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                data[j][i] = 0;
            }
        }
        fireTableDataChanged();
    }

    public int max(){
        int max = data[0][0];
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                if(data[j][i] > max)
                    max = data[j][i];
            }
        }
        return max;
    }


    public int min(){
        int min = data[0][0];
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                if(data[j][i] < min)
                    min = data[j][i];
            }
        }
        return min;
    }


    public double average(){
        double avg = 0.0;
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                avg += (int) data[j][i];
            }
        }
        return avg/25.0;
    }


    public int sum(){
        int sum = 0;
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<5;j++)
            {
                sum += data[j][i];
            }
        }
        return sum;
    }
}
