
import com.l2fprod.common.swing.JOutlookBar;
import org.freixas.jcalendar.JCalendarCombo;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
/**
 * Klasa odpowiedzialna za widok
 */
public class MyView extends JFrame {
    MyModel model;
    JLabel xLabel, yLabel;
    private JPanel p1, p2,p3, p4, p5, p6,p7,p8,p9;
    JComboBox<String> comboBox;
    JTextField jTextField = new JTextField();
    JTextArea textArea = new JTextArea();
    JTextArea ta = new JTextArea();
    JSlider sliderX = new JSlider(1,5,1);
    JSlider sliderY = new JSlider(1,5,1);
    JTable table;
    JButton restartButton = new JButton("zeruj");
    JButton setValueButton = new JButton("wstaw wartosc");
    JButton saveButton = new JButton("Zapisz");
    JMenuBar jmb = new JMenuBar();
    JMenu plik = new JMenu("Plik");
    JMenu edycja = new JMenu("Edycja");
    JMenuItem mi, previousNumber,mi3;
    JOutlookBar outlookBar;
    JCalendarCombo calendarCombo = new JCalendarCombo();
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MMM-d");
    JFreeChart freeChart;
    DefaultPieDataset pieDataset = new DefaultPieDataset();
    ChartPanel chartPanel;

    /**
     * inicjalizuje model oraz wywołuje metody
     * @param model a MyModel class
     */
    MyView(MyModel model){
        this.model = model;
        initComponents();
        setLayouts();
        pack();
        frame();
        setResizable(false);
    }

    /**
     * ustawia frame size, defaultOperation, Title, Location and Visibility
     */
    void frame() {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("Piotr Cyba zad1");
        this.setVisible(true);
    }

    /**
     * inicjalizuje i ustawia komponenty
     */
    void initComponents(){
        HashMap<String,Integer> tempMap = new HashMap<>();
        tempMap.put("0",25);
        updateChart(tempMap);

        comboBox = new JComboBox<>(model.getActions());
        mi = new JMenuItem("Zamknij");
        previousNumber = new JMenuItem("Poprzednia cyfra");
        mi3 = new JMenuItem("");
        xLabel = new JLabel("X: 0");
        yLabel = new JLabel("Y: 0");
        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();
        p4 = new JPanel();
        p5 = new JPanel();
        p6 = new JPanel();
        p7 = new JPanel();
        p8 = new JPanel();
        p9 = new JPanel();

        outlookBar = new JOutlookBar();
        outlookBar.addTab("Operacje",comboBox);

        table = new JTable(model.getTableModel());
        table.setPreferredSize(new Dimension(500,200));
        table.setRowHeight(40);

        calendarCombo.setDateFormat(date);
        jTextField.setPreferredSize(new Dimension(50,20));
        sliderX.setMajorTickSpacing(1);
        sliderX.setPaintLabels(true);
        sliderX.setOrientation(SwingConstants.HORIZONTAL);
        sliderY.setInverted(true);
        sliderY.setMajorTickSpacing(1);
        sliderY.setPaintLabels(true);
        sliderY.setOrientation(SwingConstants.VERTICAL);
        previousNumber.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
        edycja.add(previousNumber);
        plik.add(mi);
        jmb.add(plik);
        jmb.add(edycja);
    }

    /**
     * tworzy layouty i wkłada do nich komponenty
     */
    void setLayouts(){
        this.setLayout(new FlowLayout());
        p1.setLayout(new BorderLayout(0,0));
        p2.setLayout(new GridLayout(1,3,30,1));
        p3.setLayout(new GridLayout(1,3,6,1));
        p4.setLayout(new BorderLayout(25,25));
        p5.setLayout(new BorderLayout(25,25));
        p6.setLayout(new GridLayout(1,2,30,1));
        p7.setLayout(new GridLayout(1,2,30,1));
        p8.setLayout(new GridLayout(1,2,30,1));
        p9.setLayout(new GridLayout(1,2,-50,-0));

        p1.add(BorderLayout.CENTER, table);
        p1.add(BorderLayout.SOUTH,sliderX);
        p1.add(BorderLayout.EAST,sliderY);

        p2.add(restartButton);
        p2.add(setValueButton);
        p2.add(saveButton);

        p9.add(yLabel);
        p9.add(xLabel);

        p3.add(jTextField);
        p3.add(p9);
        p3.add(textArea);

        p4.add(BorderLayout.CENTER,p1);
        p4.add(BorderLayout.SOUTH,p2);
        p4.add(BorderLayout.NORTH,p3);

        p7.add(calendarCombo);
        p7.add(ta);

        p5.add(BorderLayout.CENTER,chartPanel);
        p5.add(BorderLayout.EAST,outlookBar);
        p5.add(BorderLayout.NORTH,p7);

        p6.add(p5);
        p6.add(p4);

        this.setJMenuBar(jmb);
        this.add(p6);
    }

    /**
     * aktualizuje pieChart za pomoca otrzymanych danych
     * @param map mapa z danymi potrzebnymi do aktualizacji pieChart
     */
    public void updateChart(HashMap<String,Integer> map) {
        String key;
        Iterator<String> iterator;
        iterator = map.keySet().iterator();
        pieDataset.clear();
        while (iterator.hasNext()){
            key = iterator.next();
            pieDataset.setValue(key,map.get(key));
        }

        freeChart = createChart(pieDataset);
        chartPanel = new ChartPanel(freeChart);
        chartPanel.setPreferredSize(new Dimension(150,150));
    }

    /**
     * Tworzy pie chart z otrzymanych danych
     * @param dataset  dataset potrzebny do utworzenia pieChart
     * @return new chart
     */
    private JFreeChart createChart( PieDataset dataset ) {
        JFreeChart chart = ChartFactory.createPieChart(
                "Numbers",
                dataset,
                true,
                true,
                false);
        return chart;
    }
}
