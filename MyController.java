
import com.l2fprod.common.swing.JTipOfTheDay;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Scanner;

 /**
 * klasa odpowiedzialna za funkcje kontrolera
 */
public class MyController {
    Boolean extraPool = true;
    JTipOfTheDay tipOfTheDay = new JTipOfTheDay();
    ArrayDeque<String> undo = new ArrayDeque<>();
    MyView view;
    MyModel model;


    /**
     *Inicjalizuje klasy MyModel i MyView
     * @param model  klasa odpowiedziala za model
     */
    MyController(MyModel model){
        this.view = new MyView(model);
        this.model = model;
        listeners();
        printResult();
        showTipOfTheDay();
    }

    /**
     * ustawienie listenrow
     */
    private void listeners(){
        view.setValueButton.addActionListener((ae) -> {
            int y = view.sliderY.getValue() - 1;
            int x = view.sliderX.getValue() - 1;

            String enteredVal = view.jTextField.getText();
            int val = 0;

            try {
                val = Integer.parseInt(enteredVal);
                if(undo.size() == 0 || !enteredVal.equals(undo.getLast())) {
                    undo.add(enteredVal);
                    extraPool = true;
                }
            } catch (NumberFormatException exc) {
                JOptionPane.showMessageDialog(null,
                        "Tylko cyfry sa dozwolone");
            }
            model.setValue(val,x,y);
            printResult();

        });
        view.saveButton.addActionListener((ae)-> writeToFile());
        view.restartButton.addActionListener((ae) -> {
            model.setZeros();
            printResult();});
        view.comboBox.addActionListener((ae)->printResult());
        view.sliderX.addChangeListener(ce -> view.xLabel.setText("X : " + (view.sliderX.getValue()-1)));
        view.sliderY.addChangeListener(ce -> view.yLabel.setText("Y :" + (view.sliderY.getValue()-1)));
        view.mi.addActionListener(al -> System.exit(0));
        view.previousNumber.addActionListener(al -> {
            if(undo.size() != 0 && extraPool) {
                undo.pollLast();
                extraPool = false;
            }
            view.jTextField.setText(undo.pollLast());
        });
        view.calendarCombo.addDateListener((de) -> {
            String date = view.calendarCombo.getDate().toString();
            String day = date.substring(0,3);
            String month = date.substring(4,7);
            String res ="";

            res += date.substring(25,29) + " ";
            switch (month){
                case "Jan":
                    res += "Styczeń ";
                    break;
                case "Feb":
                    res += "Luty ";
                    break;
                case "Mar":
                    res += "Marzec ";
                    break;
                case "Apr":
                    res += "Kwiecień ";
                    break;
                case "May":
                    res += "Maj ";
                    break;
                case "Jun":
                    res += "Czerwiec ";
                    break;
                case "Jul":
                    res += "Lipiec ";
                    break;
                case "Aug":
                    res += "Sierpień ";
                    break;
                case "Sep":
                    res += "Wrzesień ";
                    break;
                case "Oct":
                    res += "Październik ";
                    break;
                case "Nev":
                    res += "Listopad ";
                    break;
                case "Dec":
                    res += "Grudzień ";
                    break;
            }
            res += date.substring(8,10) + " ";
            switch (day){
                case "Mon":
                    res += "Poniedziałek ";
                    break;
                case "Tue":
                    res += "Wtorek";
                    break;
                case "Wen":
                    res += "Środa ";
                    break;
                case "Thu":
                    res += "Czwartek";
                    break;
                case "Fri":
                    res += "Piątek ";
                    break;
                case "Sat":
                    res += "Sobota ";
                    break;
                case "Sun":
                    res += "Niedziela ";
                    break;
            }

            view.ta.setText(res);
        });
    }


    /**
     * wyswietla wynik wybranej opcji
     */
    private void printResult(){
        if(view.comboBox.getSelectedIndex() == 2)
            view.textArea.setText("min = "+ model.min()+ " max= "+ model.max());
        else if(view.comboBox.getSelectedIndex() == 1)
            view.textArea.setText("srednia = " + model.average());
        else
            view.textArea.setText("suma = " + model.sum());

        view.updateChart(getChartData());
    }

    /**
     * wpisuje do pliku kazda wartosc z kazdej komorki z JTable
     * writes to file every value form JTable cells
     */
    private void writeToFile() {
        String results = "";
        String newResult = "";

        for(int i=0;i<5;i++) {
            for(int j =0;j<5;j++) {
                newResult += view.table.getValueAt(j,i);
                newResult += ',';
            }
        }
        try(Scanner scanner = new Scanner(new File("results.txt"))) {
            while (scanner.hasNext())
            {
                results += scanner.next();
                results += '\n';
            }
        }catch (FileNotFoundException exc){ System.out.println("The file \"results.txt\" " +
                "has been created"); }

        try(FileWriter fw = new FileWriter("results.txt")) {
            fw.write(results + newResult);
        }catch (IOException exc){ System.out.println("An error occurred"); }
    }

    /**
     * @return zwraca kazda unikalna wartosc z Jtbale oraz jej czestotliwosc wystepowania
     */
    HashMap<String,Integer> getChartData(){
        HashMap<String,Integer> map = new HashMap<>();
        String key = "";
        int val = 0;
        for(int i=0;i<5;i++){
            for(int j=0;j<5;j++){
                key = model.getValue(i,j) + "";
                if(map.containsKey(key)){
                    val = map.get(key);
                    map.replace(key,++val);
                }else{
                    map.put(key+"",1);
                }
            }
        }
        return map;
    }
    /**
     * pokazuje tip of the day
     */
    public void showTipOfTheDay(){
        tipOfTheDay.add(new JLabel(model.getRandomTip()));
        JOptionPane.showMessageDialog(null,tipOfTheDay);
    }
}