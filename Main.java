import java.io.IOException;
import org.apache.log4j.*;

/**
 * @author me
 * @version 3.0
 * created 05.28.2020 */

/**
 * glowna Klasa
 */
public class Main {

    public static void main(String[] args) {

        PatternLayout patternLayout = new PatternLayout("%-5p [%t]: %d%m%n");
        ConsoleAppender capp = new ConsoleAppender(new SimpleLayout());
        FileAppender fapp = null;
        try{ fapp= new FileAppender(patternLayout,"fapp.log");
        }catch (IOException exc){
            exc.getStackTrace();
        }

        Logger logger = Logger.getLogger("logger");
        logger.addAppender(capp);
        logger.setLevel(Level.INFO);
        Logger myLogger = Logger.getLogger("myLogger");
        myLogger.addAppender(fapp);
        myLogger.setLevel(Level.WARN);

        logger.info("START");

        MyModel model = new MyModel();
        new MyController(model);

    }
}
